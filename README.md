# Everyonecancontribute.dev

Source for https://everyonecancontribute.dev - a fun demo website created by [@dnsmichi](https://dnsmichi.at/about) for this job application as Developer Evangelist at GitLab in December 2019.

![Screenshot](everyonecancontribute.dev.png)

The goal was to show how to monitor HTTP and TLS certificates using [Prometheus](https://o11y.love/topics/metrics/#prometheus), and surprise the audience with a GitLab-related demo and domain ([presentation slides](https://docs.google.com/presentation/d/1RGJcrEIHp7gMfsC8YxHncSv9KFehOU1JwiQr_SEdkdc/present?slide=id.g75da1773a5_0_0), [Talk archive](https://dnsmichi.at/talks/)).

The original site everyonecancontribute.com shown in the presentation, was repurposed for the EveryoneCanContribute Cafe meetup group, moving the demo to everyonecancontribute.dev. Instead of a Linux VM, this archive is deployed using GitLab Pages.


## Development

Edit [public/index.html](public/index.html) and run a local webserver.

```
cd public

npx serve
```

## Thanks

- Clippy.js from https://github.com/pi0/clippyjs
- Animate.css from https://animate.style/ 
- GitLab animated CSS with SVG from https://codepen.io/heyMP/pen/LNjeOM 
- GitLab for the logo (before brand refresh in 2022)
- Microsoft, for creating Clippy :)